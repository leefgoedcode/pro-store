import React from "react";
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import CadastroUsuario from './pages/Cadastro/Usuario';
import {Link} from 'react-router-dom';
import Config from './pages/config';

export default () => {
    return (
        <Switch>
            <Route exact path='/'>
                <Home />
            </Route>

            <Route exact path='/login'>
                <Login />
            </Route>

            <Route exact path='/cadastro'>
                <CadastroUsuario />
            </Route>

            <Route exact path='/config'>
                <Config />
            </Route>

            
        </Switch>
    );
}