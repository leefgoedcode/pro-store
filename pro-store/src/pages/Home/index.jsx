import React from "react";
import "bootstrap-icons/font/bootstrap-icons.css";
import { ContainerPage, TitlePage } from "../../components/Main";
import { Header } from "../../components/Header";
import { SlideCarousel } from "../../components/Carousel";
import { Produts } from "../../components/Produts";

const Page = () => {
  return (
    <ContainerPage>
      <Header />
      <SlideCarousel />
      <Produts />
    </ContainerPage>
  );
};

export default Page;
