import React from "react";
import { useState } from "react";
import { SaveUser } from "../../../components/SaveUser";
import { TableUsers } from "../../../components/TableUsers";
import { ContainerPage, TitlePage } from "../../../components/Main";

const Page = () => {
  const [list, setList] = useState([]);

  function handleSaveUser(user) {
    let newList = [...list];
    newList.push(user);
    setList(newList);
  }

  return (
    <>
      <ContainerPage>
        <TitlePage>Cadastro</TitlePage>
        <SaveUser onSaveUser={handleSaveUser} />
        <TableUsers list={list} />
      </ContainerPage>
    </>
  );
};

export default Page;
