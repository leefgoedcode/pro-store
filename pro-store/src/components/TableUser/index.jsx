import {TableRow, TableCell} from '@mui/material';

export const TableUser = ({name, email}) => {
    return (
        <TableRow>
            <TableCell align='right'>{name}</TableCell>
            <TableCell align='right'>{email}</TableCell>
        </TableRow>
    )
}