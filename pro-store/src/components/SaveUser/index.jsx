import { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Form, Card } from "react-bootstrap";

export const SaveUser = ({ onSaveUser }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  function handleSaveUser() {
    const data = {
      name,
      email,
    };
    console.log(data);
    onSaveUser(data);
  }

  return (
    <>
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-4 offset-md-4">
            <Card>
              <Card.Header>
                <Card.Title className="text-center">PRO-STORE</Card.Title>
                <Card.Subtitle className="mb-2 text-muted text-center">
                  Criar minha conta
                </Card.Subtitle>
              </Card.Header>
              <Card.Body>
                <Form>
                  <Form.Group className="mb-3" controlId="formCadNome">
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Digite o nome"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formCadEmail">
                    <Form.Label>E-mail:</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <Form.Text className="text-muted text-center">
                      <div className="text-center">
                        <small>
                          Nunca compartilharemos seu e-mail com mais ninguém.
                        </small>
                      </div>
                    </Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formCadPassword">
                    <Form.Label>Senha:</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formCadCheckbox">
                    <Form.Check type="checkbox" label="Lembrar meus dados" />
                  </Form.Group>
                  <div className="d-grid gap-2">
                    <Button
                      className="btn-block"
                      variant="success"
                      type="button"
                      onClick={handleSaveUser}
                    >
                      Criar minha conta
                    </Button>
                  </div>
                </Form>
              </Card.Body>
              <Card.Footer className="text-muted text-center">
                Já é nosso cliente? Clica aqui!
              </Card.Footer>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
};
