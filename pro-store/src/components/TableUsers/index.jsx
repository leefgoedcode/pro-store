import * as React from 'react';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Table, TableBody, TableContainer } from '@mui/material';
import Paper from '@mui/material/Paper';
import { TableUser } from '../TableUser';


export const TableUsers = ({list}) => {

  return (
      <TableContainer component={Paper}>
          <Table sx={{minWidth: 650}} size="small" aria-label="simple table">
            <TableHead>
                <TableRow>
                    <TableCell align='right'> Nome</TableCell>
                    <TableCell align='right'> Email</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {list?.map((user,index) => (
                    <TableUser key={index} name={user.name} email={user.email} />
                ))}
            </TableBody>
          </Table>
    </TableContainer>
  );
}
