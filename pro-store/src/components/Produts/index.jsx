import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../Produts/style.scss";
import api from "../../services/api";

export const Produts = () => {
  const [produts, setProduts] = useState([]);
  useEffect(() => {
    api.get("/products").then(({ data }) => {
      setProduts(data.data);
    });
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  console.log(produts);

  return (
    <div className="wrapper">
      <div className="d-md-flex align-items-md-center">
        <div className="h3">Produtos mais vendidos na DHL</div>
      </div>
      <div className="d-lg-flex align-items-lg-center pt-2">
        <div className="form-inline d-flex align-items-center my-2 mr-lg-2 radio bg-light border">
          <label className="options">
            Most Popular <input type="radio" name="radio" />
            <span className="checkmark"></span>
          </label>
          <label className="options">
            Cheapest <input type="radio" name="radio" />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="form-inline d-flex align-items-center my-2 checkbox bg-light border mx-lg-2">
          <label className="tick">
            Farm <input type="checkbox" />
            <span className="check"></span>
          </label>
          <span className="text-success px-2 count"> 328</span>
        </div>
        <div className="form-inline d-flex align-items-center my-2 checkbox bg-light border mx-lg-2">
          <label className="tick">
            Bio <input type="checkbox" /> <span className="check"></span>
          </label>
          <span className="text-success px-2 count"> 72</span>
        </div>
        <div className="form-inline d-flex align-items-center my-2 checkbox bg-light border mx-lg-2">
          <label className="tick">
            Czech republic <input type="checkbox" />
            <span className="check"></span>
          </label>
          <span className="border px-1 mx-2 mr-3 font-weight-bold count">
            12
          </span>
          <select name="country" id="country" className="bg-light">
            <option value="" hidden>
              Country
            </option>
            <option value="India">India</option>
            <option value="USA">USA</option>
            <option value="Uk">UK</option>
          </select>
        </div>
      </div>
      <div className="d-sm-flex align-items-sm-center pt-2 clear">
        <div className="text-muted filter-label">Applied Filters:</div>
        <div className="green-label font-weight-bold p-0 px-1 mx-sm-1 mx-0 my-sm-0 my-2">
          Selected Filtre <span className=" px-1 close">&times;</span>
        </div>
        <div className="green-label font-weight-bold p-0 px-1 mx-sm-1 mx-0">
          Selected Filtre <span className=" px-1 close">&times;</span>
        </div>
      </div>
      <div className="filters">
        <button
          className="btn btn-success"
          type="button"
          data-toggle="collapse"
          data-target="#mobile-filter"
          aria-expanded="true"
          aria-controls="mobile-filter"
        >
          Filter<span className="px-1 fas fa-filter"></span>
        </button>
      </div>
      <div id="mobile-filter">
        <div className="py-3">
          <h5 className="font-weight-bold">Categories</h5>
          <ul className="list-group">
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
              vegetables
              <span className="badge badge-primary badge-pill">328</span>
            </li>
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
              Fruits <span className="badge badge-primary badge-pill">112</span>
            </li>
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
              Kitchen Accessories
              <span className="badge badge-primary badge-pill">32</span>
            </li>
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
              Chefs Tips
              <span className="badge badge-primary badge-pill">48</span>
            </li>
          </ul>
        </div>
        <div className="py-3">
          <h5 className="font-weight-bold">Brands</h5>
          <form className="brand">
            <div className="form-inline d-flex align-items-center py-1">
              <label className="tick">
                Royal Fields <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-1">
              <label className="tick">
                Crasmas Fields <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-1">
              <label className="tick">
                Vegetarisma Farm <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-1">
              <label className="tick">
                Farmar Field Eve <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-1">
              <label className="tick">
                True Farmar Steve <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
          </form>
        </div>
        <div className="py-3">
          <h5 className="font-weight-bold">Rating</h5>
          <form className="rating">
            <div className="form-inline d-flex align-items-center py-2">
              <label className="tick">
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span> <input type="checkbox" />
                <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-2">
              <label className="tick">
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <input type="checkbox" /> <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-2">
              <label className="tick">
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <input type="checkbox" /> <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-2">
              <label className="tick">
                <span className="fas fa-star"></span>
                <span className="fas fa-star"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <input type="checkbox" /> <span className="check"></span>
              </label>
            </div>
            <div className="form-inline d-flex align-items-center py-2">
              <label className="tick">
                <span className="fas fa-star"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <span className="far fa-star px-1 text-muted"></span>
                <input type="checkbox" /> <span className="check"></span>
              </label>
            </div>
          </form>
        </div>
      </div>
      <div className="content py-md-0 py-3">
        <section id="sidebar">
          <div className="py-3">
            <h5 className="font-weight-bold">Categories</h5>
            <ul className="list-group">
              <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
                vegetables
                <span className="badge badge-primary badge-pill">328</span>
              </li>
              <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
                Fruits{" "}
                <span className="badge badge-primary badge-pill">112</span>
              </li>
              <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
                Kitchen Accessories
                <span className="badge badge-primary badge-pill">32</span>
              </li>
              <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center category">
                Chefs Tips
                <span className="badge badge-primary badge-pill">48</span>
              </li>
            </ul>
          </div>
          <div className="py-3">
            <h5 className="font-weight-bold">Brands</h5>
            <form className="brand">
              <div className="form-inline d-flex align-items-center py-1">
                <label className="tick">
                  Royal Fields <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-1">
                <label className="tick">
                  Crasmas Fields <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-1">
                <label className="tick">
                  Vegetarisma Farm <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-1">
                <label className="tick">
                  Farmar Field Eve <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-1">
                <label className="tick">
                  True Farmar Steve <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
            </form>
          </div>
          <div className="py-3">
            <h5 className="font-weight-bold">Rating</h5>
            <form className="rating">
              <div className="form-inline d-flex align-items-center py-2">
                <label className="tick">
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>{" "}
                  <input type="checkbox" />
                  <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-2">
                <label className="tick">
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <input type="checkbox" /> <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-2">
                <label className="tick">
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <input type="checkbox" /> <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-2">
                <label className="tick">
                  <span className="fas fa-star"></span>
                  <span className="fas fa-star"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <input type="checkbox" /> <span className="check"></span>
                </label>
              </div>
              <div className="form-inline d-flex align-items-center py-2">
                <label className="tick">
                  <span className="fas fa-star"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <span className="far fa-star px-1 text-muted"></span>
                  <input type="checkbox" /> <span className="check"></span>
                </label>
              </div>
            </form>
          </div>
        </section>
        <section id="products">
          <div className="container py-3">
            <div className="row">
              {produts?.map((product) => (
                <div
                  className="col-lg-4 col-md-6 col-sm-10 offset-md-0 offset-sm-1 pt-md-0 pt-4"
                  key={product.id}
                  id={product.id}
                >
                  <div className="single-product product-1">
                    <div
                      className="part-1"
                      style={{
                        backgroundImage: `url(${product.attributes.link_image})`,
                      }}
                    >
                      <ul>
                        <li>
                          <a href="#">
                            <i className="fas fa-shopping-cart"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fas fa-heart"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fas fa-plus"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fas fa-expand"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="part-2">
                      <div className="card-body">
                        <h6 className="font-weight-bold pt-1">
                          {product.attributes.title}
                        </h6>
                        <div className="text-muted description">
                          {product.attributes.description}
                        </div>
                        <div className="d-flex align-items-center product">
                          <span className="fas fa-star"></span>
                          <span className="fas fa-star"></span>
                          <span className="fas fa-star"></span>
                          <span className="fas fa-star"></span>
                          <span className="far fa-star"></span>
                        </div>
                        <div className="d-flex align-items-center justify-content-between pt-3">
                          <div className="d-flex flex-column">
                            <h4 className="product-old-price">R$ {product.attributes.price}</h4>
                            <div className="h6 font-weight-bold">R$ {product.attributes.price_promotional}</div>
                          </div>
                          <div className="btn btn-primary">Comprar</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};
